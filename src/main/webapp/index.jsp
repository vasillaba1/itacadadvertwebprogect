<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <header>
        <jsp:include page="header.jsp" />
    </header>
    <main role="main">
<%--        <jsp:include page="/AdvertServlet"/>--%>
    </main>
    <footer></footer>
</body>
</html>