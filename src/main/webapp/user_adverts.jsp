<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 2/24/2021
  Time: 11:10 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<header>
    <jsp:include page="header.jsp" />
</header>
<button onclick="window.location.href = 'add_advert.jsp';">Add my advert</button>
<div class="container">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Title</th>
                <th scope="col">Text</th>
                <th scope="col">Date of creating</th>
                <th scope="col">heading</th>
                <th scope="col">Is active</th>
            </tr>
        </thead>
        <tbody>

            <c:forEach items="${requestScope.useradverts}" var="advert">
                <form action="EditAdvertServlet" method="get">
                    <input hidden type="text" name="id" value="${advert.id}" readonly />
                    <tr>
                        <td><c:out value="${advert.title}"/></td>
                        <td><c:out value="${advert.text}"/></td>
                        <td><c:out value="${advert.dateOfCreating}"/></td>
                        <td><c:out value="${advert.heading}"/></td>
                        <td><c:out value="${advert.isActive}"/></td>
                        <td>
                            <button type="submit">Edit</button></form>
                        </td>
                    </form>
            </c:forEach>
            </tr>
        </tbody>
    </table>
</div>

</body>
</html>
