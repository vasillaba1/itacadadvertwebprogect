<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 2/24/2021
  Time: 11:00 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Header</title>
    <style>
        ul#menu li {
            display:inline;
        }
    </style>
</head>
<body>
<div class="container">
    <a href="AdvertServlet" class="logo"><u><h1>ADVERTS.COM</h1></u></a>
    <a href="ShowMyAdvertServlet" class="logo"><u><h3>     My Adverts</h3></u></a>
    <link rel="stylesheet" href="css/style.css">
    <nav>
        <ul>
            <li><a href="UserServlet">     All Users     |</a></li>
            <c:if test="${cookie.userLogin.value!=null}">
                <li><p>Hello <c:out value="${cookie.userLogin.value}" /></p></li>
                <li><a href="LogOutServlet">/  Log out!</a></li>
            </c:if>
            <c:if test="${cookie.userLogin.value==null}">
                <li><a href="login.jsp">     LogIn  /</a></li>
                <li><a href="registration.jsp">  Registration</a></li>
            </c:if>
        </ul>
    </nav>
</div>
</body>
</html>
