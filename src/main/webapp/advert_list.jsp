<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 2/24/2021
  Time: 11:10 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Adverts</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<header>
    <jsp:include page="header.jsp" />
</header>
<main role="main">
    <div class="container">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Title</th>
                <th scope="col">Text</th>
                <th scope="col">Date of creating</th>
                <th scope="col">heading</th>

            </tr>
            </thead>
            <tbody>
            <c:set var="i" value="1" scope="request"/>
            <c:forEach items="${requestScope.adverts}" var="advert">
            <tr>
                <td><c:out value="${advert.title}"/></td>
                <td><c:out value="${advert.text}"/></td>
                <td><c:out value="${advert.dateOfCreating}"/></td>
                <td><c:out value="${advert.heading}"/></td>
                <td>
                </td>
                </c:forEach>
            </tr>
            </tbody>
        </table>
    </div>
</main>

</body>
</html>
