<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 3/2/2021
  Time: 10:43 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <title>Add davert</title>
</head>
<body>
<form action="AddNewAdvertServlet" method="post">
        <p>
            <label for="title1">Input title</label>
            <input type="text" id="title1" name="title1">
        </p>
        <p>
            <label for="heading">Choose heading:</label>
            <select id="heading" name="heading">
                <option value="GAMES">GAMES</option>
                <option value="NEWS">NEWS</option>
                <option value="IT">IT</option>
                <option value="JAVA">JAVA</option>
                <option value="LIFE">LIFE</option>
                <option value="OTHER">OTHER</option>
            </select>
        </p>
        <p>
            <label for="text">Input your advert:</label><br/>
            <textarea id="text" name="text" placeholder="Write text" width="60%" rows="15"></textarea>
        </p>
        <p>
            <label for="active">Is active:</label><br/>
            <p>
                <input type="radio" value="true" checked name="active" id="active"/>active
            </p>
            <p>
                <input type="radio" value="false" name="active" />not active
            </p>
        </p>
    <p>
        <input type="submit" value="Add" />
    </p>
</form>
</body>
</html>