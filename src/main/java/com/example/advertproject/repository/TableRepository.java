package com.example.advertproject.repository;

import com.example.advertproject.config.DBConnector;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class TableRepository {

    private TableRepository() {
    }

    public static void refreshDB() throws IOException, SQLException {
        dropTableAdverts();
        dropTableUsers();
        createTableUsers();
        createTableAdverts();
        fillTableUsers();
        fillTableAdverts();

    }


    public static boolean createTableUsers() throws IOException, SQLException {
        final Connection connection = DBConnector.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement("CREATE TABLE Users" +
                "(Id SERIAL PRIMARY KEY,\n" +
                "    login VARCHAR(30) NOT NULL,\n" +
                "    email VARCHAR(20) NOT NULL,\n" +
                "    password VARCHAR(250) NOT NULL,\n" +
                "    birthday DATE DEFAULT '1999-01-01',\n" +
                "\trole VARCHAR(15) NOT NULL DEFAULT 'USER'" +
                ");"))
        {
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e){
            return false;
        }finally {
            connection.close();
        }
    }
    public static boolean createTableAdverts() throws IOException, SQLException {
        final Connection connection = DBConnector.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement("CREATE TABLE Adverts" +
                "(Id SERIAL PRIMARY KEY," +
                "    AuthorId INTEGER NOT NULL REFERENCES Users(Id) ON DELETE CASCADE," +
                "    title VARCHAR(30) DEFAULT 'without topics'," +
                "    adverttext TEXT," +
                "    datecreating DATE DEFAULT '1999-01-01'," +
                "    heading VARCHAR(30) DEFAULT 'LIFE'," +
                "    isActive boolean DEFAULT true);"))
        {
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e){
            return false;
        }finally {
            connection.close();
        }
    }
    public static boolean dropTableAdverts() throws IOException, SQLException {
        final Connection connection = DBConnector.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement("DROP TABLE Adverts;")){
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e){
            return false;
        }finally {
            connection.close();
        }
    }
    public static boolean dropTableUsers() throws IOException, SQLException {
        final Connection connection = DBConnector.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement("DROP TABLE Users;")){
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e){
            return false;
        }finally {
            connection.close();
        }
    }
    public static boolean fillTableAdverts() throws IOException, SQLException {
        final Connection connection = DBConnector.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO Adverts  (AuthorId, title, adverttext)" +
                "VALUES" +
                "(1, 'Hello world 2', 'OTHER')," +
                "(2, 'Hello world 2', 'OTHER')," +
                "(1, 'Hello world ', 'OTHER');")){
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e){
            return false;
        }finally {
            connection.close();
        }
    }
    public static boolean fillTableUsers() throws IOException, SQLException {
        final Connection connection = DBConnector.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement("" +
                "INSERT INTO Users  (login, email, password)" +
                "VALUES" +
                "('Vasil', 'vasillaba1@gmail.com', '111111')," +
                "('Andrew', 'wizardben@gmail.com', '222222')," +
                "('Viktoria', 'vkramskaia@gmail.com', '333333');")){
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e){
            return false;
        }finally {
            connection.close();
        }
    }

}
