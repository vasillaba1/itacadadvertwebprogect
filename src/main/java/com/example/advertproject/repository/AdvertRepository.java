package com.example.advertproject.repository;

import com.example.advertproject.config.DBConnector;
import com.example.advertproject.model.Advert;
import com.example.advertproject.model.Role;
import com.example.advertproject.model.Heading;
import com.example.advertproject.model.User;

import java.io.IOException;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class AdvertRepository {
    private AdvertRepository() {
    }

    public static Advert getAdvertById(Long idSearch) throws SQLException, IOException {
        Advert advert = new Advert();
        final Connection connection = DBConnector.getConnection();
        try (java.sql.PreparedStatement statement = connection.prepareStatement("SELECT users.Id, users.login, users.email, users.password, users.birthday, users.role,adverts.Id, adverts.title, adverts.adverttext, adverts.datecreating, adverts.heading, adverts.isActive FROM Adverts JOIN users ON users.Id = adverts.authorid WHERE adverts.Id = (?)"))
        {
            statement.setLong(1, idSearch);
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                User user = new User(
                        resultSet.getLong(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getDate(5).toLocalDate(),
                        Role.valueOf(resultSet.getString(6)));
                advert.setUser(user);
                advert.setId(resultSet.getLong(7));
                advert.setTitle(resultSet.getString(8));
                advert.setText(resultSet.getString(9));
                advert.setDateOfCreating(resultSet.getDate(10).toLocalDate());
                advert.setHeading(Heading.valueOf(resultSet.getString(11)));
                advert.setActive(resultSet.getBoolean(12));
            }

        } finally{
            connection.close();
        }
        return advert;
    }
    public static List<Advert> getAdvertByUserId(Long idSearch) throws SQLException, IOException {
        List<Advert> advertsByUserIdList = new ArrayList<>();
        Advert advert;
        final Connection connection = DBConnector.getConnection();
        try (java.sql.PreparedStatement statement = connection.prepareStatement("" +
                "SELECT Id, AuthorId, title, adverttext, datecreating, heading, isActive FROM Adverts WHERE AuthorId = (?)"))
        {
            statement.setLong(1, idSearch);
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                advert = new Advert();
                advert.setId(resultSet.getLong(1));
                advert.setTitle(resultSet.getString(3));
                advert.setText(resultSet.getString(4));
                advert.setDateOfCreating(resultSet.getDate(5).toLocalDate());
                advert.setHeading(Heading.valueOf(resultSet.getString(6)));
                advert.setActive(resultSet.getBoolean(7));
                advertsByUserIdList.add(advert);
            }

        } finally {
            connection.close();
        }
        return advertsByUserIdList;
    }
    public static boolean setAdvert(Advert advert) throws IOException, SQLException {
        final Connection connection = DBConnector.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO Adverts ( AuthorId, title, adverttext,datecreating,heading, isActive) VALUES (?,?,?,?,?,?);")) {
            preparedStatement.setLong(1, advert.getUser().getId());
            preparedStatement.setString(2, advert.getTitle());
            preparedStatement.setString(3, advert.getText());
            preparedStatement.setDate(4, Date.valueOf(LocalDate.now()) /*Date.valueOf( advert.getDateOfCreating())*/);
            preparedStatement.setString(5, advert.getHeading().name());
            preparedStatement.setBoolean(6, advert.getIsActive());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e){
            return false;
        }finally {
            connection.close();
        }
    }
    public static boolean updateAdvert(Advert advert) throws IOException, SQLException {
        final Connection connection = DBConnector.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement("UPDATE Adverts SET title = ?, adverttext = ?, datecreating = ?, heading = ?, isactive = ? WHERE id = ?;")) {
            preparedStatement.setString(1, advert.getTitle());
            preparedStatement.setString(2, advert.getText());
            preparedStatement.setDate(3, Date.valueOf(LocalDate.now()));
            preparedStatement.setString(4, advert.getHeading().name());
            preparedStatement.setBoolean(5, advert.getIsActive());
            preparedStatement.setLong(6, advert.getId());
            preparedStatement.executeUpdate();
            return true;
        }catch (SQLException e){
            return false;
        } finally {
            connection.close();
        }
    }

    public static boolean deleteAdvert(Long idDelete) throws IOException, SQLException {
        final Connection connection = DBConnector.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement("delete from Adverts WHERE Id=?")) {
            preparedStatement.setLong(1,idDelete);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    public static List<Advert> getAllAdverts() throws SQLException, IOException {
        List<Advert> advertList = new ArrayList<>();
        final Connection connection = DBConnector.getConnection();
        try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM adverts WHERE isactive = true;")) {
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Advert advert = new Advert();
                advert.setId(resultSet.getLong(1));
                //  advert.setUserId(resultSet.getLong(2));
                advert.setTitle(resultSet.getString(3));
                advert.setText(resultSet.getString(4));
                advert.setDateOfCreating(resultSet.getDate(5).toLocalDate());
                advert.setHeading(Heading.valueOf(resultSet.getString(6)));
                advert.setActive(resultSet.getBoolean(7));
                advertList.add(advert);
            }

        } finally {
            connection.close();
        }
        return advertList;
    }
}