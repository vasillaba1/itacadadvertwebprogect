package com.example.advertproject.repository;

import com.example.advertproject.config.DBConnector;
import com.example.advertproject.model.Role;
import com.example.advertproject.model.User;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static com.example.advertproject.services.HashPassword.mdFive;

public final class UserRepository {

    private UserRepository() {
    }

    public static User getUserById(Long idSearch) throws SQLException, IOException {
        User user = new User();
        final Connection connection = DBConnector.getConnection();
        try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM users WHERE Id = (?)")) {
            statement.setLong(1, idSearch);
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                user.setId(resultSet.getLong(1));
                user.setLogin(resultSet.getString("login"));
                user.setEmail(resultSet.getString(3));
                user.setPassword(resultSet.getString(4));
                user.setBirthDay(resultSet.getDate(5).toLocalDate());
                user.setRole(Role.valueOf(resultSet.getString(6)));
            }

        } finally {
            connection.close();
        }
        return user;
    }

    public static User getUserByLogin(String login) throws SQLException, IOException {
        User user = new User();
        final Connection connection = DBConnector.getConnection();
        try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM users WHERE login = (?)")) {
            statement.setString(1, login);
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                user.setId(resultSet.getLong(1));
                user.setLogin(resultSet.getString("login"));
                user.setEmail(resultSet.getString(3));
                user.setPassword(resultSet.getString(4));
                user.setBirthDay(resultSet.getDate(5).toLocalDate());
                user.setRole(Role.valueOf(resultSet.getString(6)));
            }

        } finally {
            connection.close();
        }
        return user;
    }

    public static User getUsersSingIn(String login, String password) throws SQLException, IOException {
        User user = new User();
        final Connection connection = DBConnector.getConnection();
        try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM users WHERE login = (?) And password = (?)")) {
            statement.setString(1, login);
            statement.setString(2, mdFive(password));
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                user.setId(resultSet.getLong(1));
                user.setLogin(resultSet.getString("login"));
                user.setEmail(resultSet.getString(3));
                user.setPassword(resultSet.getString(4));
                user.setBirthDay(resultSet.getDate(5).toLocalDate());
                user.setRole(Role.valueOf(resultSet.getString(6)));
            }

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } finally {
            connection.close();
        }
        return user;
    }


    public static boolean setUser(User user) throws IOException, SQLException {
        final Connection connection = DBConnector.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO Users  (login, email, password, birthday, role) VALUES (?,?,?,?,?);")) {
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getEmail());
            preparedStatement.setString(3, user.getHashPass());
            preparedStatement.setDate(4, Date.valueOf(user.getBirthDay()));
            preparedStatement.setString(5, user.getRole().name());
            preparedStatement.executeUpdate();
            return true;
        }  catch (SQLException e){
            return false;
        }finally {
            connection.close();
        }
    }
    public static boolean setUserRegister(User user) throws IOException, SQLException {
        final Connection connection = DBConnector.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO Users  (login, email, password) VALUES (?,?,?);")) {
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getEmail());
            preparedStatement.setString(3, user.getHashPass());
            preparedStatement.executeUpdate();
            return true;
        }  catch (SQLException e){
            return false;
        }finally {
            connection.close();
        }
    }

    public static boolean updateUser(Long id, User user) throws IOException, SQLException {
        final Connection connection = DBConnector.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement("UPDATE Users SET login = ?, email = ?, password = ?, birthday = ? WHERE id = ?;")) {
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getEmail());
            preparedStatement.setString(3, user.getHashPass());
            preparedStatement.setDate(4, Date.valueOf(user.getBirthDay()));
            preparedStatement.setLong(5, id);
            preparedStatement.executeUpdate();
            return true;
        }catch (SQLException e){
            return false;
        } finally {
            connection.close();
        }
    }

    public static boolean deleteUser(Long idDelete) throws IOException, SQLException {
        final Connection connection = DBConnector.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement("delete from Users WHERE Id=?")) {
            preparedStatement.setLong(1,idDelete);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

    public static List<User> getAllUsers() throws SQLException, IOException {
        List<User> userList = new ArrayList<>();
        final Connection connection = DBConnector.getConnection();
        try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM users")) {
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                User user = new User();
                user.setId(resultSet.getLong(1));
                user.setLogin(resultSet.getString("login"));
                user.setEmail(resultSet.getString(3));
                user.setPassword(resultSet.getString(4));
                user.setBirthDay(resultSet.getDate(5).toLocalDate());
                user.setRole(Role.valueOf(resultSet.getString(6)));
                userList.add(user);
            }

        } finally {
            connection.close();
        }
        return userList;
    }
}
