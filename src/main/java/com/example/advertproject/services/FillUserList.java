package com.example.advertproject.services;

import com.example.advertproject.model.User;
import com.example.advertproject.repository.AdvertRepository;

import java.io.IOException;
import java.sql.SQLException;

public class FillUserList {
    public User fillList(User user) throws IOException, SQLException {
        user.setAdvertsList(AdvertRepository.getAdvertByUserId(user.getId()));
        return user;
    }
}
