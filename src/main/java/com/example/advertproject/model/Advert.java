package com.example.advertproject.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.time.LocalDate;

public class Advert {
    private long id;
    private User user;
    @Min(1)
    @Max(200)
    private String title;
    private String text;
    private LocalDate dateOfCreating;
    private Heading heading;
    private boolean isActive;

    public Advert() {
    }

    public Advert(User user,@Min(1) @Max(200) String title, String text, LocalDate dateOfCreating, Heading heading, boolean isActive) {
        this.user = user;
        this.title = title;
        this.text = text;
        this.dateOfCreating = dateOfCreating;
        this.heading = heading;
        this.isActive = isActive;
    }
    public Advert(@Min(1) @Max(200) String title, String text, LocalDate dateOfCreating, Heading heading, boolean isActive) {
        this.title = title;
        this.text = text;
        this.dateOfCreating = dateOfCreating;
        this.heading = heading;
        this.isActive = isActive;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDate getDateOfCreating() {
        return dateOfCreating;
    }

    public void setDateOfCreating(LocalDate dateOfCreating) {
        this.dateOfCreating = dateOfCreating;
    }

    public Heading getHeading() {
        return heading;
    }

    public void setHeading(Heading heading) {
        this.heading = heading;
    }

    public boolean getIsActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    @Override
    public String toString() {
        return "Advert{" +
                "id=" + id +
                ", author=" + user +
                ", title='" + title + '\'' +
                ", text='" + text + '\'' +
                ", dateOfCreating=" + dateOfCreating +
                ", heading=" + heading +
                ", isActive=" + isActive +
                '}'+"\n";
    }
}
