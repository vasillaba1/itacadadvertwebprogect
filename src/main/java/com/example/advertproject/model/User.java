package com.example.advertproject.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.util.List;

import static com.example.advertproject.services.HashPassword.mdFive;

public class User {
    private long id;
    @NotNull(message="Login must be not null")
    @Size(min = 2, max = 30, message="Login should be  between 2 and 30 characters")
    private String login;
    @NotNull(message="Email must be not null")
    @Email(message = "Email must be valid")
    private String email;
    @NotNull(message="password must be not null")
    @Size(min = 2, max = 30, message="password must have 2 - 30 symbol")
    private String password;
    private LocalDate birthDay;
    private Role role;
    private List<Advert> advertsList;

    public User() {
    }

    public User(long id, String login, String email, String password,
                LocalDate birthDay, Role role) {
        this.id = id;
        this.login = login;
        this.email = email;
        this.password = password;
        this.birthDay = birthDay;
        this.role = role;
    }
    public User(String login, String email, String password) {
        this.login = login;
        this.email = email;
        this.password = password;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public List<Advert> getAdvertsList() {
        return advertsList;
    }

    public void setAdvertsList(List<Advert> advertsList) {
        this.advertsList = advertsList;
    }

    public String getHashPass(){
        try {
            return mdFive(password);
        } catch (NoSuchAlgorithmException e) { e.printStackTrace(); }
        return "hash error";
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", birthDay=" + birthDay +
                ", role=" + role +
                ", adverts=" + advertsList +
                '}'+"\n";
    }

}