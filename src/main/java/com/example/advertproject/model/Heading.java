package com.example.advertproject.model;

public enum Heading {
    GAMES, NEWS, IT, JAVA, OTHER, LIFE
}
