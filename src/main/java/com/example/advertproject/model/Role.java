package com.example.advertproject.model;

public enum Role {
    USER, ADMIN, UNAUTHORIZED
}
