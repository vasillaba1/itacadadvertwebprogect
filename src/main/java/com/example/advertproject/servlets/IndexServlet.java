package com.example.advertproject.servlets;

import com.example.advertproject.model.User;
import com.example.advertproject.repository.UserRepository;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "IndexServlet", value = "/IndexServlet")
public class IndexServlet extends HttpServlet {



    public IndexServlet() throws IOException, SQLException {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getServletPath();
        switch (action) {
            case "/userList":
                try { userList(request, response); } catch (SQLException e) { e.printStackTrace(); }
                break;
            case "/insert":
               // insertAdvert(request, response);
                break;
        }
    }

    private void userList(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
        request.setAttribute("users", UserRepository.getAllUsers());
        RequestDispatcher dispatcher = request.getRequestDispatcher("/webapp/user_list.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
