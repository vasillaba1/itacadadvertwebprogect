package com.example.advertproject.servlets;

import com.example.advertproject.repository.AdvertRepository;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "AdvertServlet", value = "/AdvertServlet")
public class AdvertServlet extends HttpServlet {

    public AdvertServlet() throws IOException, SQLException {
    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            request.setAttribute("adverts", AdvertRepository.getAllAdverts());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher("/advert_list.jsp");
        dispatcher.forward(request, response);
    }
}

