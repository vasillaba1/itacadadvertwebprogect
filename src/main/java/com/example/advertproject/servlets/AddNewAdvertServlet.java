package com.example.advertproject.servlets;

import com.example.advertproject.model.Advert;
import com.example.advertproject.model.Heading;
import com.example.advertproject.model.User;
import com.example.advertproject.repository.AdvertRepository;
import com.example.advertproject.repository.UserRepository;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;

import static java.lang.Boolean.parseBoolean;

@WebServlet(name = "AddNewAdvertServlet", value = "/AddNewAdvertServlet")
public class AddNewAdvertServlet extends HttpServlet {
    private String message;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.sendRedirect("/Gradle___com_example___advertproject_1_2_1_0_SNAPSHOT_war/ShowMyAdvertServlet");

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String title = request.getParameter("title1");
        String text = request.getParameter("text");
        String heading = request.getParameter("heading");
        String active = request.getParameter("active");

        Cookie[] cookies = request.getCookies();
        String login = null;
        String cookieName = "userLogin";
        if(cookies !=null) {
            for(Cookie c: cookies) {
                if(cookieName.equals(c.getName())) {
                    login = c.getValue();
                    break;
                }
            }
        }
        try { ;
            User logUser = UserRepository.getUserByLogin(login);
            Advert advert = new Advert(logUser,title,text,LocalDate.now(), Heading.valueOf(heading),parseBoolean(active));
            AdvertRepository.setAdvert(advert);
            doGet(request, response);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
