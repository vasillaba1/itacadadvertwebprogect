package com.example.advertproject.servlets;

import com.example.advertproject.model.User;
import com.example.advertproject.repository.AdvertRepository;
import com.example.advertproject.repository.UserRepository;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet(name = "ShowMyAdvertServlet", value = "/ShowMyAdvertServlet")
public class ShowMyAdvertServlet extends HttpServlet {

    public ShowMyAdvertServlet() throws IOException, SQLException {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Cookie[] cookies = request.getCookies();
        String login = null;
        String password = null;
        String cookieName = "userLogin";
        Cookie cookie = null;
        if(cookies !=null) {
            for(Cookie c: cookies) {
                if(cookieName.equals(c.getName())) {
                    login = c.getValue();
                    break;
                }
            }
        }
        try { ;
            User logUser = UserRepository.getUserByLogin(login);
            request.setAttribute("useradverts", AdvertRepository.getAdvertByUserId(logUser.getId()));
            RequestDispatcher dispatcher = request.getRequestDispatcher("/user_adverts.jsp");
            dispatcher.forward(request, response);
        } catch (SQLException e) {
            e.printStackTrace();
        }



    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}