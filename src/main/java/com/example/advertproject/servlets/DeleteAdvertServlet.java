package com.example.advertproject.servlets;

import com.example.advertproject.repository.AdvertRepository;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "DeleteAdvertServlet", value = "/DeleteAdvertServlet")
public class DeleteAdvertServlet extends HttpServlet {
    private String message;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.sendRedirect("/Gradle___com_example___advertproject_1_2_1_0_SNAPSHOT_war/ShowMyAdvertServlet");

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String id = request.getParameter("id");

        try {
            AdvertRepository.deleteAdvert(Long.parseLong(id));
            doGet(request, response);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
