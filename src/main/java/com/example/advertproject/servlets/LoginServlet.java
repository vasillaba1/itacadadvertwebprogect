package com.example.advertproject.servlets;

import com.example.advertproject.model.Role;
import com.example.advertproject.model.User;
import com.example.advertproject.repository.AdvertRepository;
import com.example.advertproject.repository.UserRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.time.LocalDate;

@WebServlet(name = "LoginServlet", value = "/LoginServlet")
public class LoginServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String login = request.getParameter("login");
        String password = request.getParameter("password");

        try {
            ServletContext servletContext = getServletContext();
            User logUser = UserRepository.getUsersSingIn(login,password);

            if (logUser.getLogin()==null){
                RequestDispatcher dispatcher = servletContext.getRequestDispatcher("/login.jsp");
                dispatcher.forward(request, response);
            }

            if (logUser.getLogin().equals(login)){
                request.setAttribute("message", "Login successful ))");
                response.addCookie(new Cookie("userLogin", login));
                RequestDispatcher dispatcher = servletContext.getRequestDispatcher("/registration_succesfull.jsp");
                dispatcher.forward(request, response);
            }

        } catch (SQLException e) { e.printStackTrace(); }
    }
}
