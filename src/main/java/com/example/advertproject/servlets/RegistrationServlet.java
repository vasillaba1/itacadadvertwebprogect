package com.example.advertproject.servlets;

import com.example.advertproject.model.Role;
import com.example.advertproject.model.User;
import com.example.advertproject.repository.AdvertRepository;
import com.example.advertproject.repository.UserRepository;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

@WebServlet(name = "registrationServlet", value = "/registrationServlet")
public class RegistrationServlet extends HttpServlet {
    private String message;

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String login = request.getParameter("login");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String birthDay = request.getParameter("birthDay");
        LocalDate bDay = LocalDate.parse(birthDay);

        User user = new User(1l,login,email,password, bDay, Role.USER);

        try {
            ServletContext servletContext = getServletContext();
            UserRepository.setUser(user);
            request.setAttribute("message", "Registration successful");
            RequestDispatcher dispatcher = servletContext.getRequestDispatcher("/registration_succesfull.jsp");
            dispatcher.forward(request, response);
        } catch (SQLException e) { e.printStackTrace(); }
    }
}
