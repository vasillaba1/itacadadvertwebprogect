package com.example.advertproject.config;
import java.sql.DriverManager;
import org.postgresql.Driver;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnector {
    public static Connection getConnection() throws SQLException, IOException {
//        Properties props = new Properties();
//        try(InputStream in = Files.newInputStream(Paths.get("src/main/resources/templates/database.properties"))){
//            props.load(in);
//        }
        String url = "jdbc:postgresql://localhost/advertSite";//props.getProperty("url");
        String username = "postgres";//props.getProperty("username");
        String password = "postgres";//props.getProperty("password");

        DriverManager.registerDriver(new Driver());


        return DriverManager.getConnection(url, username, password);
    }
}
