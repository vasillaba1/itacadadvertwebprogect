package com.example.advertproject.main;

import com.example.advertproject.model.Role;
import com.example.advertproject.model.User;
import com.example.advertproject.repository.TableRepository;
import com.example.advertproject.repository.UserRepository;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

import static com.example.advertproject.repository.TableRepository.*;

public class Main {
    public static void main(String[] args) throws IOException, SQLException {

//        System.out.println("Hello");
//        User user = new User(20l,"MD5Uasr", "MD5Uasr@sf.df", "MD5Uasrsd", LocalDate.now(), Role.USER);
//        UserRepository.setUser(user);
//        System.out.println(UserRepository.getAllUsers());
//        System.out.println(UserRepository.getUsersSingIn("MD5Uasr","MD5Uasrsd"));
        dropTableAdverts();
        createTableAdverts();
        fillTableAdverts();


    }
}
